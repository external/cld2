This mirror has been deprecated. The actual repository has moved to github:
https://github.com/CLD2Owners/cld2 . The new mirror of it is:
https://chromium.googlesource.com/external/github.com/CLD2Owners/cld2 .
